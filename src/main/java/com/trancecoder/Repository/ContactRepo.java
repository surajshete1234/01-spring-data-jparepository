package com.trancecoder.Repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trancecoder.dataobject.contactDetails;


public interface ContactRepo extends JpaRepository<contactDetails, Serializable> {

}
