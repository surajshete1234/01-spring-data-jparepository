package com.trancecoder;


import java.awt.print.Pageable;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.domain.Sort;

import com.trancecoder.Repository.ContactRepo;
import com.trancecoder.dataobject.contactDetails;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctxt = SpringApplication.run(Application.class, args);

		ContactRepo conrepo = ctxt.getBean(ContactRepo.class);

	
		List<contactDetails> list = conrepo.findAll(Sort.by("clientNum").descending());
		
		list.forEach( li -> {
			System.out.println(li);
		});

	}

}
